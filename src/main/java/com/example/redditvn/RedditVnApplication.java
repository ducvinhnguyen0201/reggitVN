package com.example.redditvn;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RedditVnApplication {

    public static void main(String[] args) {
        SpringApplication.run(RedditVnApplication.class, args);
    }

}
