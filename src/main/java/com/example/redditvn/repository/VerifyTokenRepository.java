package com.example.redditvn.repository;

import com.example.redditvn.model.VerificationToken;
import org.springframework.data.jpa.repository.JpaRepository;

public interface VerifyTokenRepository extends JpaRepository<VerificationToken, Long> {
}
